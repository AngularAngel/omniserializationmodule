
module omni.serialization {
    requires static lombok;
    requires devil.util;
    requires java.logging;
    requires omni.module;
    exports net.angle.omniserialization.api;
    exports net.angle.omniserialization.impl;
}