package net.angle.omniserialization.impl;

import com.samrj.devil.util.Bufferable;
import java.nio.ByteBuffer;
import java.util.function.Supplier;
import lombok.RequiredArgsConstructor;
import net.angle.omniserialization.api.PrimitiveSerializer;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public class BufferableSerializer<T extends Bufferable> implements PrimitiveSerializer<T> {
    private final Supplier<T> constructor;

    @Override
    public T readPrimitive(ByteBuffer buffer) {
        T instance = constructor.get();
        instance.read(buffer);
        return instance;
    }

    @Override
    public int getSizeOfPrimitive(T t) {
        return t.bufferSize();
    }

    @Override
    public void writePrimitive(T t, ByteBuffer buffer) {
        t.write(buffer);
    }
}