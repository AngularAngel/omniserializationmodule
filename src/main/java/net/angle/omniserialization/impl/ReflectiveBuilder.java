package net.angle.omniserialization.impl;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.Objects;
import lombok.ToString;
import net.angle.omniserialization.api.Builder;

/**
 *
 * @author angle
 */
@ToString
public class ReflectiveBuilder<T> extends AbstractBuilder<T> {
    private final Constructor<T> constructor;
    private final Object[] arguments;
    
    public ReflectiveBuilder(Constructor<T> constructor) {
        super(constructor.getDeclaringClass());
        this.constructor = constructor;
        arguments = new Object[constructor.getParameters().length];
    }
    
    public void setArgument(int index, Object object) {
        if (object == this)
            throw new IllegalArgumentException("Cannot set an object's builder as it's own argument. Class: " + constructor.getDeclaringClass() + ", Index: " + index);
            
        try {
            checkSettability();
            arguments[index] = object;
        } catch(Exception ex) {
            throw new IllegalStateException("Builder for " + constructor.getDeclaringClass() + " cannot set index " + index + " To " + object, ex);
        }
    }
    
    @Override
    protected boolean isReady() {
        Parameter[] parameters = constructor.getParameters();
        for (int i = 0; i < parameters.length; i++)
            if (Arrays.stream(parameters[i].getAnnotations()).anyMatch((t) -> {
                return t.annotationType().getName().equals("NonNull");
            }) && Objects.isNull(arguments[i]))
                return false;
        return true;
    }

    @Override
    public T build() {
        //new Exception(constructor.getDeclaringClass().toString()).printStackTrace();
        checkBuildability();
        for (int i = 0; i < arguments.length; i++) {
            arguments[i] = finalizeArgument(arguments[i]);
        }
        try {
            return build(constructor.newInstance(arguments));
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            throw new IllegalArgumentException("Problem building: " + getObjectClass(), ex);
        }
    }
}