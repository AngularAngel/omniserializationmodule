package net.angle.omniserialization.impl;

import com.samrj.devil.util.IOUtil;
import java.nio.ByteBuffer;
import java.util.function.IntFunction;
import lombok.RequiredArgsConstructor;
import net.angle.omniserialization.api.PrimitiveSerializer;
import net.angle.omniserialization.api.PrimitiveSerializerRegistry;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public class PrimitiveArraySerializer<T> implements PrimitiveSerializer<T[]> {
    private final PrimitiveSerializerRegistry registry;
    private final Class<T> componentClass;
    private final IntFunction<T[]> constructor;
    
    @Override
    public T[] readPrimitive(ByteBuffer buffer) {
        int length = IOUtil.readVLQ(buffer);
        if (length < 0) {
            return null;
        }
        
        T[] array = constructor.apply(length);
        for (int i = 0; i < length; i++)
            array[i] = registry.readPrimitive(componentClass, buffer);
        return array;
    }

    @Override
    public void writePrimitive(T[] array, ByteBuffer buffer) {
        IOUtil.writeVLQ(buffer, array.length);
        for (T t : array)
            registry.writePrimitive(t, buffer);
    }

    @Override
    public int getSizeOfPrimitive(T[] array) {
        int size = IOUtil.sizeOfVLQ(array.length);
        if (array.length > 0)
            size += registry.getSizeOfPrimitive(array[0]) * array.length;
        return size;
    }
    
}
