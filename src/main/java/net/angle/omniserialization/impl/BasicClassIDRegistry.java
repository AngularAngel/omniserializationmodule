package net.angle.omniserialization.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Map;
import net.angle.omniserialization.api.ClassIDRegistry;

/**
 *
 * @author angle
 */
public class BasicClassIDRegistry implements ClassIDRegistry {

    private final Map<Class<?>, Integer> classIDs = new IdentityHashMap<>(); //give each class an id for serialization
    private final Map<Integer, Class<?>> classes = new HashMap<>();
    
    @Override
    public int addClass(Class<?> cls) {
        int id = classIDs.size();
        classes.put(id, cls);
        classIDs.put(cls, id);
        return id;
    }
    
    @Override
    public int getID(Class cls) {
        return classIDs.get(cls);
    }

    @Override
    public Class getClass(int id) {
        return classes.get(id);
    }

    @Override
    public Collection<Class<?>> getClasses() {
        return classes.values();
    }
    
}