package net.angle.omniserialization.impl;

import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Objects;
import net.angle.omniserialization.api.ClassIDRegistry;
import net.angle.omniserialization.api.PrimitiveSerializer;
import net.angle.omniserialization.api.PrimitiveSerializerRegistry;

/**
 *
 * @author angle
 */
public class BasicPrimitiveSerializerRegistry implements PrimitiveSerializerRegistry {
    private final Map<Class<?>, PrimitiveSerializer<?>> serializers = new IdentityHashMap<>();
    private final ClassIDRegistry classRegistry = new BasicClassIDRegistry();
    
    protected void addComponentClass(Class<?> cls) {
        classRegistry.addClass(cls);
    }

    @Override
    public int getComponentID(Class<?> primitiveClass) {
        return classRegistry.getID(primitiveClass);
    }

    @Override
    public <T> boolean containsPrimitive(Class<T> cls) {
        return serializers.containsKey(cls);
    }

    @Override
    public <T> Class<T> getComponentClass(int primitiveID) {
        return (Class<T>) classRegistry.getClass(primitiveID);
    }

    @Override
    public <T> void registerPrimitive(Class<T> primitiveClass, PrimitiveSerializer<T> serializer) {
        PrimitiveSerializer<T> existingSerializer = getPrimitiveSerializer(primitiveClass);
        if (Objects.nonNull(existingSerializer))
            throw new IllegalArgumentException("Cannot register class: " + primitiveClass + ", already registered for: " + existingSerializer);
        addComponentClass(primitiveClass);
        serializers.put(primitiveClass, serializer);
    }

    @Override
    public <T> PrimitiveSerializer<T> getPrimitiveSerializer(Class<T> cls) {
        return (PrimitiveSerializer<T>) serializers.get(cls);
    }
}