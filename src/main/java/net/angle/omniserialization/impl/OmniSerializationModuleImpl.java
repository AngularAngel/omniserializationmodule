package net.angle.omniserialization.impl;

import com.samrj.devil.math.Mat3;
import com.samrj.devil.math.Mat4;
import com.samrj.devil.math.Vec2;
import com.samrj.devil.math.Vec2i;
import com.samrj.devil.math.Vec3;
import com.samrj.devil.math.Vec3i;
import com.samrj.devil.math.Vec4;
import com.samrj.devil.util.IOUtil;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.angle.omnimodule.impl.BasicOmniModule;
import net.angle.omniserialization.api.Builder;
import net.angle.omniserialization.api.OmniSerializationModule;
import net.angle.omniserialization.api.ObjectSerializerRegistry;
import net.angle.omniserialization.api.PrimitiveSerializer;

/**
 *
 * @author angle
 */
public class OmniSerializationModuleImpl extends BasicOmniModule implements OmniSerializationModule {
    public OmniSerializationModuleImpl() {
        super(OmniSerializationModule.class);
    }
    
    @Override
    public ObjectSerializerRegistry getSerializerRegistry() {
        BasicSerializerRegistry registry = new BasicSerializerRegistry();
        
        registry.registerPrimitives(new Class[] {byte.class, Byte.class}, new PrimitiveSerializer<Byte>() {
            @Override
            public Byte readPrimitive(ByteBuffer buffer) {
                return buffer.get();
            }

            @Override
            public void writePrimitive(Byte b, ByteBuffer buffer) {
                buffer.put(b);
            }

            @Override
            public int getSizeOfPrimitive(Byte b) {
                return 1;
            }
        });
        
        registry.registerPrimitives(new Class[] {short.class, Short.class}, new PrimitiveSerializer<Short>() {
            @Override
            public Short readPrimitive(ByteBuffer buffer) {
                return buffer.getShort();
            }

            @Override
            public void writePrimitive(Short s, ByteBuffer buffer) {
                buffer.putShort(s);
            }

            @Override
            public int getSizeOfPrimitive(Short s) {
                return 2;
            }
        });
        
        registry.registerPrimitives(new Class[] {int.class, Integer.class}, new PrimitiveSerializer<Integer>() {
            @Override
            public Integer readPrimitive(ByteBuffer buffer) {
                return buffer.getInt();
            }

            @Override
            public void writePrimitive(Integer i, ByteBuffer buffer) {
                buffer.putInt(i);
            }

            @Override
            public int getSizeOfPrimitive(Integer i) {
                return 4;
            }
        });
        
        registry.registerPrimitives(new Class[] {long.class, Long.class}, new PrimitiveSerializer<Long>() {
            @Override
            public Long readPrimitive(ByteBuffer buffer) {
                return buffer.getLong();
            }

            @Override
            public void writePrimitive(Long l, ByteBuffer buffer) {
                buffer.putLong(l);
            }

            @Override
            public int getSizeOfPrimitive(Long l) {
                return 8;
            }
        });
        
        registry.registerPrimitives(new Class[] {float.class, Float.class}, new PrimitiveSerializer<Float>() {
            @Override
            public Float readPrimitive(ByteBuffer buffer) {
                return buffer.getFloat();
            }

            @Override
            public void writePrimitive(Float f, ByteBuffer buffer) {
                buffer.putFloat(f);
            }

            @Override
            public int getSizeOfPrimitive(Float f) {
                return 4;
            }
        });
        
        registry.registerPrimitives(new Class[] {double.class, Double.class}, new PrimitiveSerializer<Double>() {
            @Override
            public Double readPrimitive(ByteBuffer buffer) {
                return buffer.getDouble();
            }

            @Override
            public void writePrimitive(Double d, ByteBuffer buffer) {
                buffer.putDouble(d);
            }

            @Override
            public int getSizeOfPrimitive(Double d) {
                return 8;
            }
        });
        
        registry.registerPrimitives(new Class[] {boolean.class, Boolean.class}, new PrimitiveSerializer<Boolean>() {
            @Override
            public Boolean readPrimitive(ByteBuffer buffer) {
                return buffer.get() != 0;
            }

            @Override
            public void writePrimitive(Boolean b, ByteBuffer buffer) {
                buffer.put(b ? (byte) 1 : (byte) 0);
            }

            @Override
            public int getSizeOfPrimitive(Boolean b) {
                return 1;
            }
        });
        
        registry.registerPrimitives(new Class[] {char.class, Character.class}, new PrimitiveSerializer<Character>() {
            @Override
            public Character readPrimitive(ByteBuffer buffer) {
                return buffer.getChar();
            }

            @Override
            public void writePrimitive(Character c, ByteBuffer buffer) {
                buffer.putChar(c);
            }

            @Override
            public int getSizeOfPrimitive(Character c) {
                return 2;
            }
        });
        
        registry.registerPrimitive(String.class, new PrimitiveSerializer<String>() {
            @Override
            public String readPrimitive(ByteBuffer buffer) {
                return IOUtil.readUTF8(buffer);
            }

            @Override
            public void writePrimitive(String string, ByteBuffer buffer) {
                IOUtil.writeUTF8(buffer, string);
            }

            @Override
            public int getSizeOfPrimitive(String string) {
                return IOUtil.sizeOfUTF8(string);
            }
        });
        
        registry.registerPrimitive(byte[].class, new PrimitiveSerializer<byte[]>() {
            @Override
            public byte[] readPrimitive(ByteBuffer buffer) {
                int length = IOUtil.readVLQ(buffer);
                if (length < 0) {
                    return null;
                }

                byte[] array = new byte[length];
                buffer.get(array);
                return array;
            }

            @Override
            public void writePrimitive(byte[] bytes, ByteBuffer buffer) {
                IOUtil.writeVLQ(buffer, bytes.length);
                buffer.put(bytes);
            }

            @Override
            public int getSizeOfPrimitive(byte[] bytes) {
                return IOUtil.sizeOfVLQ(bytes.length) + bytes.length;
            }
        });
        
        registry.registerPrimitive(short[].class, new PrimitiveSerializer<short[]>() {
            @Override
            public short[] readPrimitive(ByteBuffer buffer) {
                int length = IOUtil.readVLQ(buffer);
                if (length < 0) {
                    return null;
                }

                short[] array = new short[length];
                for (int i = 0; i < length; i++)
                    array[i] = buffer.getShort();
                return array;
            }

            @Override
            public void writePrimitive(short[] shorts, ByteBuffer buffer) {
                IOUtil.writeVLQ(buffer, shorts.length);
                for (short s : shorts)
                    buffer.putShort(s);
            }

            @Override
            public int getSizeOfPrimitive(short[] shorts) {
                return IOUtil.sizeOfVLQ(shorts.length) + 2 * shorts.length;
            }
        });
        
        registry.registerPrimitive(int[].class, new PrimitiveSerializer<int[]>() {
            @Override
            public int[] readPrimitive(ByteBuffer buffer) {
                int length = IOUtil.readVLQ(buffer);
                if (length < 0) {
                    return null;
                }

                int[] array = new int[length];
                for (int i = 0; i < length; i++)
                    array[i] = buffer.getInt();
                return array;
            }

            @Override
            public void writePrimitive(int[] ints, ByteBuffer buffer) {
                IOUtil.writeVLQ(buffer, ints.length);
                for (int i : ints)
                    buffer.putInt(i);
            }

            @Override
            public int getSizeOfPrimitive(int[] ints) {
                return IOUtil.sizeOfVLQ(ints.length) + 4 * ints.length;
            }
        });
        
        registry.registerPrimitive(long[].class, new PrimitiveSerializer<long[]>() {
            @Override
            public long[] readPrimitive(ByteBuffer buffer) {
                int length = IOUtil.readVLQ(buffer);
                if (length < 0) {
                    return null;
                }

                long[] array = new long[length];
                for (int i = 0; i < length; i++)
                    array[i] = buffer.getLong();
                return array;
            }

            @Override
            public void writePrimitive(long[] longs, ByteBuffer buffer) {
                IOUtil.writeVLQ(buffer, longs.length);
                for (long l : longs)
                    buffer.putLong(l);
            }

            @Override
            public int getSizeOfPrimitive(long[] longs) {
                return IOUtil.sizeOfVLQ(longs.length) + 8 * longs.length;
            }
        });
        
        registry.registerPrimitive(float[].class, new PrimitiveSerializer<float[]>() {
            @Override
            public float[] readPrimitive(ByteBuffer buffer) {
                int length = IOUtil.readVLQ(buffer);
                if (length < 0) {
                    return null;
                }

                float[] array = new float[length];
                for (int i = 0; i < length; i++)
                    array[i] = buffer.getFloat();
                return array;
            }

            @Override
            public void writePrimitive(float[] floats, ByteBuffer buffer) {
                IOUtil.writeVLQ(buffer, floats.length);
                for (float f : floats)
                    buffer.putFloat(f);
            }

            @Override
            public int getSizeOfPrimitive(float[] floats) {
                return IOUtil.sizeOfVLQ(floats.length) + 4 * floats.length;
            }
        });
        
        registry.registerPrimitive(double[].class, new PrimitiveSerializer<double[]>() {
            @Override
            public double[] readPrimitive(ByteBuffer buffer) {
                int length = IOUtil.readVLQ(buffer);
                if (length < 0) {
                    return null;
                }

                double[] array = new double[length];
                for (int i = 0; i < length; i++)
                    array[i] = buffer.getDouble();
                return array;
            }

            @Override
            public void writePrimitive(double[] doubles, ByteBuffer buffer) {
                IOUtil.writeVLQ(buffer, doubles.length);
                for (double d : doubles)
                    buffer.putDouble(d);
            }

            @Override
            public int getSizeOfPrimitive(double[] doubles) {
                return IOUtil.sizeOfVLQ(doubles.length) + 8 * doubles.length;
            }
        });
        
        registry.registerPrimitive(boolean[].class, new PrimitiveSerializer<boolean[]>() {
            @Override
            public boolean[] readPrimitive(ByteBuffer buffer) {
                int numBits = IOUtil.readVLQ(buffer);
                if (numBits < 0)
                    return null;

                int numBytes = (numBits + 7) / 8;
                byte[] bytes = new byte[numBytes];
                buffer.get(bytes);
                BitSet bits = BitSet.valueOf(bytes);
                boolean[] array = new boolean[numBits];
                for (int i = 0; i < numBits; i++)
                    array[i] = bits.get(i);
                return array;
            }

            @Override
            public void writePrimitive(boolean[] booleans, ByteBuffer buffer) {
                int byteCount = (booleans.length + 7)/8;
                IOUtil.writeVLQ(buffer, booleans.length);
                BitSet bits = new BitSet(booleans.length);
                for (int i = 0; i < booleans.length; i++) if (booleans[i]) bits.set(i);
                byte[] bytes = bits.toByteArray();
                buffer.put(bytes);
                for (int i = bytes.length; i < byteCount; i++) buffer.put((byte) 0); //Must pad since BitSet chops off trailing zeroes.
            }

            @Override
            public int getSizeOfPrimitive(boolean[] booleans) {
                return IOUtil.sizeOfVLQ(booleans.length) + (booleans.length + 7)/8;
            }
        });
        
        registry.registerPrimitive(char[].class, new PrimitiveSerializer<char[]>() {
            @Override
            public char[] readPrimitive(ByteBuffer buffer) {
                int length = IOUtil.readVLQ(buffer);
                if (length < 0) {
                    return null;
                }

                char[] array = new char[length];
                for (int i = 0; i < length; i++)
                    array[i] = buffer.getChar();
                return array;
            }

            @Override
            public void writePrimitive(char[] chars, ByteBuffer buffer) {
                IOUtil.writeVLQ(buffer, chars.length);
                for (char c : chars)
                    buffer.putChar(c);
            }

            @Override
            public int getSizeOfPrimitive(char[] chars) {
            return IOUtil.sizeOfVLQ(chars.length) + 2 * chars.length;
            }
        });
        registry.registerPrimitive(int[][].class, new PrimitiveArraySerializer<>(registry, int[].class, int[][]::new));
        registry.registerPrimitive(int[][][].class, new PrimitiveArraySerializer<>(registry, int[][].class, int[][][]::new));
        registry.registerPrimitive(long[][].class, new PrimitiveArraySerializer<>(registry, long[].class, long[][]::new));
        registry.registerPrimitive(long[][][].class, new PrimitiveArraySerializer<>(registry, long[][].class, long[][][]::new));
        
        
        registry.registerPrimitive(Vec4.class, new BufferableSerializer<>(Vec4::new));
        registry.registerPrimitive(Vec3i.class, new BufferableSerializer<>(Vec3i::new));
        registry.registerPrimitive(Vec3.class, new BufferableSerializer<>(Vec3::new));
        registry.registerPrimitive(Vec2i.class, new BufferableSerializer<>(Vec2i::new));
        registry.registerPrimitive(Vec2.class, new BufferableSerializer<>(Vec2::new));
        registry.registerPrimitive(Mat3.class, new BufferableSerializer<>(Mat3::new));
        registry.registerPrimitive(Mat4.class, new BufferableSerializer<>(Mat4::new));
        
        try {
            registry.registerObject(ArrayList.class, new ReflectiveSerializer<>(ArrayList.class.getConstructor(), (instance) -> {
                Object[] ret = new Object[instance.size() * 2 + 1];
                ret[0] = instance.size();
                int i = 1;
                for (Object obj : instance) {
                    ret[i++] = registry.getComponentID(obj.getClass());
                    ret[i++] = obj;
                }
                return ret;
            }) {
                @Override
                public void readComponents(ObjectSerializerRegistry registry, ReflectiveBuilder<ArrayList> builder, ArrayList<Builder> builders, ByteBuffer buffer) {
                    int length = registry.readPrimitive(int.class, buffer);
                    if (length == 0)
                        return;
                    Object[] objects = new Object[length];
                    for (int i = 0; i < length; i++)
                        objects[i] = registry.readComponent(registry.readPrimitive(int.class, buffer), builders, buffer);
                    builder.addModification((t) -> {
                        for (Object obj: objects)
                            if (obj instanceof Builder builderObj)
                                t.add(builderObj.getObject());
                            else
                                t.add(obj);
                    });
                }
            });
        } catch (NoSuchMethodException | SecurityException ex) {
            Logger.getLogger(OmniSerializationModuleImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        registry.registerObject(Object[].class, new ArraySerializer<>(Object.class, Object[]::new) {
            @Override
            public Object[] getComponents(Object[] instance) {
                Object[] ret = new Object[instance.length * 2 + 1];
                ret[0] = instance.length;
                int i = 1;
                for (Object obj : instance) {
                    ret[i++] = registry.getComponentID(obj.getClass());
                    ret[i++] = obj;
                }
                return ret;
            }

            @Override
            public void readComponents(ObjectSerializerRegistry registry, ArrayBuilder<Object[]> builder, ArrayList<Builder> builders, ByteBuffer buffer) {
                int length = registry.readPrimitive(int.class, buffer);
                if (length == 0)
                    return;
                for (int i = 0; i < length; i++)
                    builder.values.add(registry.readComponent(registry.readPrimitive(int.class, buffer), builders, buffer));
            }
            
        });
        return registry;
    }
}