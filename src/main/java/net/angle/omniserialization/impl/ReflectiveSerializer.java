package net.angle.omniserialization.impl;

import java.lang.reflect.Constructor;
import java.lang.reflect.Parameter;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.function.Function;
import lombok.RequiredArgsConstructor;
import net.angle.omniserialization.api.Builder;
import net.angle.omniserialization.api.ObjectSerializer;
import net.angle.omniserialization.api.ObjectSerializerRegistry;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public class ReflectiveSerializer<T> implements ObjectSerializer<T, ReflectiveBuilder<T>> {
    private final Constructor<T> constructor;
    private final Function<T, Object[]> componentGetter;
    
    @Override
    public ReflectiveBuilder<T> instantiateBuilder() {
        return new ReflectiveBuilder<>(constructor);
    }

    @Override
    public void readComponents(ObjectSerializerRegistry registry, ReflectiveBuilder<T> builder, ArrayList<Builder> builders, ByteBuffer buffer) {
        Parameter[] parameters = constructor.getParameters();
        for (int i = 0; i < parameters.length; i++)
            try {
                Class<?> type = parameters[i].getType();
                builder.setArgument(i, registry.readComponent(type, builders, buffer));
                
            }   catch (IllegalArgumentException ex) {
                ex.printStackTrace();
            }
    }

    @Override
    public Object[] getComponents(T instance) {
        return componentGetter.apply(instance);
    }
}