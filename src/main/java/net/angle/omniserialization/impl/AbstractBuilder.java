package net.angle.omniserialization.impl;

import java.util.ArrayList;
import java.util.function.Consumer;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import net.angle.omniserialization.api.Builder;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
@ToString
public abstract class AbstractBuilder<T> implements Builder<T> {
    private final @Getter Class<T> objectClass;
    private final ArrayList<Consumer<T>> modifications = new ArrayList<>();
    private @Setter(AccessLevel.PROTECTED) @Getter T object;
    private @Setter(AccessLevel.PROTECTED) Status status = Status.INCOMPLETE;
    
    protected abstract boolean isReady();
    
    protected void evaluateStatus() {
        if (status == Status.INCOMPLETE && isReady())
            setStatus(Status.READY);
        
        if (status == Status.READY && !isReady())
            setStatus(Status.INCOMPLETE);
    }

    @Override
    public Status getStatus() {
        evaluateStatus();
        return status;
    }

    @Override
    public void addModification(Consumer<T> modification) {
        modifications.add(modification);
    }
    
    public void checkSettability() {
        evaluateStatus();
        if (status == Status.BUILT || status == Status.FINISHED)
            throw new IllegalStateException("Can not set argument for " + object +" after building!");
    }
    
    public void checkBuildability() {
        evaluateStatus();
        if (status != Status.READY)
            throw new IllegalStateException("Builder for " + objectClass + " can only build when READY, Current Status is: " + status + "." );
    }
    
    public void checkFinishability() {
        evaluateStatus();
        if (status != Status.BUILT)
            throw new IllegalStateException("Builder for " + objectClass + " can only finish when BUILT, Current Status is: " + status);
    }
    
    public T build(T object) {
        setObject(object);
        setStatus(Status.BUILT);
        return object;
    }
    
    @Override
    public void finish() {
        checkFinishability();
        for (Consumer<T> modification : modifications)
            modification.accept(object);
        setStatus(Status.FINISHED);
    }
    
    public Object finalizeArgument(Object argument) {
        if (argument instanceof Builder builder) {
                if (builder.getStatus() != Status.BUILT)
                    builder.build();
                return builder.getObject();
        } else
            return argument;
    }
}