package net.angle.omniserialization.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.function.IntFunction;
import lombok.ToString;
import net.angle.omniserialization.api.Builder;

/**
 *
 * @author angle
 */
@ToString
public class ArrayBuilder<T> extends AbstractBuilder<T> implements Builder<T> {
    private final IntFunction<T> generator;
    public final List values = new ArrayList<>();

    public ArrayBuilder(Class<T> cls, IntFunction<T> generator) {
        super(cls);
        this.generator = generator;
    }

    @Override
    public T build() {
        checkBuildability();
        for (int i = 0; i < values.size(); i++) {
            Object object = finalizeArgument(values.get(i));
            values.set(i, object);
        }
        return build((T) values.toArray(generator));
    }

    @Override
    protected boolean isReady() {
        return true;
    }
}