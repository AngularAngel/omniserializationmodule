package net.angle.omniserialization.impl;

import com.samrj.devil.util.IOUtil;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.nio.ByteBuffer;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;
import java.util.function.Function;
import java.util.function.IntFunction;
import lombok.NonNull;
import net.angle.omniserialization.api.Builder;
import net.angle.omniserialization.api.ObjectSerializer;
import net.angle.omniserialization.api.ObjectSerializerRegistry;
import net.angle.omniserialization.api.PrimitiveSerializer;

/**
 *
 * @author angle
 */
public class BasicSerializerRegistry extends BasicPrimitiveSerializerRegistry implements ObjectSerializerRegistry {
    private final Map<Class<?>, ObjectSerializer> serializers = new IdentityHashMap<>();
    
    public BasicSerializerRegistry() {}

    @Override
    public <T> void registerObject(@NonNull Class<T> cls, @NonNull ObjectSerializer<T, ?> serializer) {
        if (serializers.containsKey(cls))
            throw new IllegalArgumentException("Duplicate class registration for " + cls.getSimpleName());
        serializers.put(cls, serializer);
        addComponentClass(cls);
    }
    
    @Override
    public <T> void registerObject(@NonNull Constructor<T> constructor,
            Function<T, Object[]> componentGetter) {
        registerObject(constructor.getDeclaringClass(), new ReflectiveSerializer<>(constructor, componentGetter));
    }
    
    @Override
    public <T> void registerObject(@NonNull Class<T[]> cls, @NonNull IntFunction<T[]> generator) {
        registerObject(cls, new ArraySerializer<>((Class<T>) cls.componentType(), generator));
    }
    
    @Override
    public <T> T[] readComponentArray(Class<T[]> arrayType, ArrayList<Builder> builders, ByteBuffer buffer) {
        int length = IOUtil.readVLQ(buffer);
        if (length == -1)
            return null;
        Class<T> componentType = (Class<T>) arrayType.componentType();
        T[] componentArray = (T[]) Array.newInstance(componentType, length);
        PrimitiveSerializer<T> primitiveSerializer = getPrimitiveSerializer(componentType);
        for (int i = 0; i < componentArray.length; i++) {
            if (Objects.nonNull(primitiveSerializer))
                componentArray[i] = primitiveSerializer.readPrimitive(buffer);
            else {
                int pointer = IOUtil.readVLQ(buffer);
                componentArray[i] = pointer != -1 ? (T) builders.get(pointer) : null;
            }
        }
        return componentArray;
    }
    
    @Override
    public <T> T readComponent(Class<T> type, ArrayList<Builder> builders, ByteBuffer buffer) {
        PrimitiveSerializer<T> primitiveSerializer = getPrimitiveSerializer(type);
        T result = null;
        try {
            if (primitiveSerializer != null) {
                result = primitiveSerializer.readPrimitive(buffer);
            } else if (type.isArray() && containsPrimitive(type.getComponentType())) {
                result = (T) readComponentArray((Class<Object[]>) type, builders, buffer);
            } else {
                int pointer = IOUtil.readVLQ(buffer);
                //throw new RuntimeException("Pointer: " + pointer);
                if (pointer == -1)
                    result = null;
                else
                    result = (T) builders.get(pointer);
            }
        } catch(Exception ex) {
            throw new IllegalArgumentException("Problem reading: " + type, ex);
        }
        //System.out.println("Reading: " + type + ", " + result);
        return result;
    }
    
    @Override
    public <T> T readComponent(int primitiveID, ArrayList<Builder> builders, ByteBuffer buffer) {
        Class<T> cls = (Class<T>) getComponentClass(primitiveID);
        return readComponent(cls, builders, buffer);
    }

    @Override
    public <T> T readObject(@NonNull ByteBuffer buffer) throws InstantiationException, IllegalAccessException {
        ArrayList<Builder> builders = new ArrayList<>();
        while (true) {
            int classID = IOUtil.readVLQ(buffer);
            if (classID == -1)
                break;
            int quantity = IOUtil.readVLQ(buffer);
            for (int i = 0; i < quantity; i++)
                builders.add(serializers.get(getComponentClass(classID)).instantiateBuilder());
        }
        for (Builder builder : builders)
            serializers.get(builder.getObjectClass()).readComponents(this, builder, builders, buffer);
        
        
        //System.out.println("Reading ObjectSet:" + builders + ", " + builders.size());
        int rootID = IOUtil.readVLQ(buffer);
        Builder<T> builder = builders.get(rootID);
        builder.build();
        
        for (Builder objectBuilder : builders)
            objectBuilder.finish();
        builders.clear();
        return (T) builder.getObject();
    }
    
    public Set<Object> getAllObjects(@NonNull Object object) {
        Set<Object> objectSet = Collections.newSetFromMap(new IdentityHashMap<>());
        Queue candidateList = new ArrayDeque();
        candidateList.add(object);
        while (!candidateList.isEmpty()) {
            Object current = candidateList.poll();
            Class<?> type = current.getClass();
            ObjectSerializer serializer;
            if (Objects.nonNull(current) && !objectSet.contains(current) && (serializer = serializers.get(type)) != null) {
                candidateList.addAll(Arrays.asList(serializer.getComponents(current)));
                objectSet.add(current);
            } else if (type.isArray() && !type.getComponentType().isPrimitive()) {
                for (Object obj : (Object[]) current)
                    candidateList.add(obj);
            }
        }
        return objectSet;
    }
    
    @Override
    public <T> void writeComponentArray(T[] componentArray, Map<Object, Integer> objPointerMap, ByteBuffer buffer) {
        if (Objects.isNull(componentArray)) {
            IOUtil.writeVLQ(buffer, -1);
            return;
        }
        Class<T[]> arrayType = (Class<T[]>) componentArray.getClass();
        Class<T> componentType = (Class<T>) arrayType.getComponentType();
        
        IOUtil.writeVLQ(buffer, componentArray.length);
            
        PrimitiveSerializer<T> primitiveSerializer = getPrimitiveSerializer(componentType);

        for (int i = 0; i < componentArray.length; i++) {
            T element = componentArray[i];

            if (primitiveSerializer != null)
                primitiveSerializer.writePrimitive(element, buffer); //Can write field as primitive.
            else
                IOUtil.writeVLQ(buffer, element != null ? objPointerMap.get(element) : -1);
        }
    }
    
    @Override
    public <T> void writeComponent(T component, Map<Object, Integer> objPointerMap, ByteBuffer buffer) {
        if (Objects.isNull(component)) {
            IOUtil.writeVLQ(buffer, -1);
            return;
        }
        Class<T> type = (Class<T>) component.getClass();
        //System.out.println("Writing: " + type + ", " + component);
        PrimitiveSerializer<T> primitiveSerializer = getPrimitiveSerializer(type);
        if (primitiveSerializer != null) {
            primitiveSerializer.writePrimitive(component, buffer); //Can write field as primitive.
            return;
        }

        if (type.isArray() && containsPrimitive(type.getComponentType())) {
            writeComponentArray((Object[]) component, objPointerMap, buffer);
            return;
        } else {
            IOUtil.writeVLQ(buffer, objPointerMap.get(component));
        }
    }
    
    public <T> int sizeOfComponentArray(T[] componentArray, Map<Object, Integer> objPointerMap) {
        int size = IOUtil.sizeOfVLQ(componentArray.length);

        for (T obj : componentArray)
            size += sizeOfComponent(obj, objPointerMap);

        return size;
    }
    
    @Override
    public <T> int sizeOfComponent(T obj, Map<Object, Integer> objPointerMap) {
        if (Objects.isNull(obj))
            return IOUtil.sizeOfVLQ(-1);
        Class<T> type = (Class<T>) obj.getClass();
        PrimitiveSerializer<T> primitiveSerializer = getPrimitiveSerializer(type);
        if (Objects.nonNull(primitiveSerializer))
            return primitiveSerializer.getSizeOfPrimitive(obj);
        if (type.isArray() && containsPrimitive(type.getComponentType())) {
            return sizeOfComponentArray((Object[]) obj, objPointerMap);
        }
        else return IOUtil.sizeOfVLQ(objPointerMap.get(obj));
    }
    
    public void writeObjectComponents(@NonNull Object object, Map<Object, Integer> objPointerMap, @NonNull ByteBuffer buffer) {
        serializers.get(object.getClass()).writeComponents(this, object, objPointerMap, buffer);
    }

    @Override
    public void writeObject(@NonNull Object root, @NonNull ByteBuffer buffer) throws IllegalAccessException {
        try {
            Map<Object, Integer> objPointerMap = new HashMap<>();
            if (Objects.isNull(root))
                throw new IllegalArgumentException("Will not serialize null root object!");

            Set<Object> objectSet = getAllObjects(root);

            //System.out.println("Writing ObjectSet:" + objectSet  + ", " + objectSet.size());

            Map<Class<?>, ArrayList> objectListMap = new IdentityHashMap<>();
            List<List> objectLists = new ArrayList<>();

            for (Object object : objectSet) {
                ArrayList objectList = objectListMap.get(object.getClass());
                if (objectList == null) {
                    objectList = new ArrayList();
                    objectListMap.put(object.getClass(), objectList);
                    objectLists.add(objectList);
                }
                objectList.add(object);
            }

            int i = 0;
            for (List objectList : objectLists) {
                for (Object object : objectList)
                    objPointerMap.put(object, i++);
            }

            for (List<Object> objectList : objectLists) {
                IOUtil.writeVLQ(buffer, getComponentID(objectList.get(0).getClass()));
                IOUtil.writeVLQ(buffer, objectList.size());
            }

            IOUtil.writeVLQ(buffer, -1);

            for (List objectList : objectLists)
                for (Object object : objectList)
                    writeObjectComponents(object, objPointerMap, buffer);

            IOUtil.writeVLQ(buffer, objPointerMap.get(root));
            objPointerMap.clear();
        } catch(Exception ex) {
            throw new RuntimeException("Error serializing " + root, ex);
        }
    }
    
    public int sizeOfObjectComponents(Object object, Map<Object, Integer> objPointerMap) {
        return serializers.get(object.getClass()).getSizeOfComponents(this, object, objPointerMap);
    }
    
    @Override
    public int sizeOfObject(Object obj) {
        try {
            Map<Object, Integer> objPointerMap = new HashMap<>();
            Set<Object> objectSet = getAllObjects(obj);

            Map<Class<?>, ArrayList> objectListMap = new IdentityHashMap<>();
            List<List> objectLists = new ArrayList<>();

            for (Object object : objectSet) {
                ArrayList objectList = objectListMap.get(object.getClass());
                if (objectList == null) {
                    objectList = new ArrayList();
                    objectListMap.put(object.getClass(), objectList);
                    objectLists.add(objectList);
                }
                objectList.add(object);
            }

            int i = 0;
            for (List objectList : objectLists) {
                for (Object object : objectList)
                    objPointerMap.put(object, i++);
            }

            int size = 0;

            for (List<Object> objectList : objectLists) {
                size += IOUtil.sizeOfVLQ(getComponentID(objectList.get(0).getClass()));
                size += IOUtil.sizeOfVLQ(objectList.size());
            }

            size += IOUtil.sizeOfVLQ(-1);

            for (List objectList : objectLists)
                for (Object object : objectList)
                    size += sizeOfObjectComponents(object, objPointerMap);

            size += IOUtil.sizeOfVLQ(objPointerMap.get(obj));
            objPointerMap.clear();
            return size;
        } catch(Exception ex) {
            throw new RuntimeException("Error sizing " + obj, ex);
        }
    }
}