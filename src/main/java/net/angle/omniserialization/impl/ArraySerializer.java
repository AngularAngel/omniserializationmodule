package net.angle.omniserialization.impl;

import com.samrj.devil.util.IOUtil;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.IntFunction;
import lombok.RequiredArgsConstructor;
import net.angle.omniserialization.api.Builder;
import net.angle.omniserialization.api.ObjectSerializer;
import net.angle.omniserialization.api.ObjectSerializerRegistry;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public class ArraySerializer<T> implements ObjectSerializer<T[], ArrayBuilder<T[]>> {
    private final Class<T> cls;
    private final IntFunction<T[]> generator;

    @Override
    public ArrayBuilder<T[]> instantiateBuilder() {
        return new ArrayBuilder<>((Class<T[]>) cls.arrayType(), generator);
    }

    @Override
    public Object[] getComponents(T[] instance) {
        Object[] ret = new Object[instance.length + 1];
        ret[0] = instance.length;
        System.arraycopy(instance, 0, ret, 1, instance.length);
        return ret;
    }

    @Override
    public void readComponents(ObjectSerializerRegistry registry, ArrayBuilder<T[]> builder, ArrayList<Builder> builders, ByteBuffer buffer) {
            int length = registry.readComponent(int.class, builders, buffer);

            for (int i = 0; i < length; i++)
                builder.values.add(registry.readComponent(cls, builders, buffer));
    }
}