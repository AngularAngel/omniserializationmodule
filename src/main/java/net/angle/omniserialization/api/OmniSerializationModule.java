package net.angle.omniserialization.api;

import net.angle.omnimodule.api.OmniModule;

/**
 *
 * @author angle
 */
public interface OmniSerializationModule extends OmniModule {
    public ObjectSerializerRegistry getSerializerRegistry();
}