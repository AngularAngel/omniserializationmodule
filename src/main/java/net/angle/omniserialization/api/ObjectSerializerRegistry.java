package net.angle.omniserialization.api;

import java.lang.reflect.Constructor;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Map;
import java.util.function.Function;
import java.util.function.IntFunction;
import lombok.NonNull;

/**
 *
 * @author angle
 */
public interface ObjectSerializerRegistry extends PrimitiveSerializerRegistry {
    public <T> void registerObject(@NonNull Class<T> cls, @NonNull ObjectSerializer<T, ?> serializer);
    public <T> void registerObject(@NonNull Constructor<T> constructor, Function<T, Object[]> componentGetter);
    public <T> void registerObject(@NonNull Class<T[]> cls, @NonNull IntFunction<T[]> generator);
    public <T> T[] readComponentArray(Class<T[]> arrayType, ArrayList<Builder> builders, ByteBuffer buffer);
    public <T> T readComponent(Class<T> type, ArrayList<Builder> builders, ByteBuffer buffer);
    public <T> T readComponent(int componentID, ArrayList<Builder> builders, ByteBuffer buffer);
    public <T> @NonNull T readObject(@NonNull ByteBuffer buffer) throws InstantiationException, IllegalAccessException;
    public <E> void writeComponentArray(E[] componentArray, Map<Object, Integer> objPointerMap, ByteBuffer buffer);
    public <E> void writeComponent(E component, Map<Object, Integer> objPointerMap, ByteBuffer buffer);
    public void writeObject(@NonNull Object instance, @NonNull ByteBuffer buffer) throws IllegalAccessException;
    public <T> int sizeOfComponent(T t, Map<Object, Integer> objPointerMap);
    public int sizeOfObject(Object obj);
}