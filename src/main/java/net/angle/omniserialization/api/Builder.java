package net.angle.omniserialization.api;

import java.util.function.Consumer;

/**
 *
 * @author angle
 */
public interface Builder<T> {
    public static enum Status {
        INCOMPLETE, READY, BUILT, FINISHED;
    }
    
    public Class<T> getObjectClass();
    public Status getStatus();
    public void addModification(Consumer<T> modification);
    public T build();
    public void finish();
    public T getObject();
}
