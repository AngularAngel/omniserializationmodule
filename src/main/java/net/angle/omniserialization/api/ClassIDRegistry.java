package net.angle.omniserialization.api;

import java.util.Collection;

/**
 *
 * @author angle
 */
public interface ClassIDRegistry {
    public int getID(Class cls);
    public Class getClass(int id);
    public int addClass(Class<?> cls);
    public Collection<Class<?>> getClasses();
}