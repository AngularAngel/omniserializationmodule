package net.angle.omniserialization.api;

import java.nio.ByteBuffer;

/**
 *
 * @author angle
 */
public interface PrimitiveSerializerRegistry {
    public <T> void registerPrimitive(Class<T> primitiveClass, PrimitiveSerializer<T> serializer);
    public default void registerPrimitives(Class[] primitiveClasses, PrimitiveSerializer serializer) {
        for (Class primitiveClass : primitiveClasses)
            registerPrimitive(primitiveClass, serializer);
    }
    public <T> boolean containsPrimitive(Class<T> cls);
    public <T> PrimitiveSerializer<T> getPrimitiveSerializer(Class<T> cls);
    public int getComponentID(Class<?> primitiveClass);
    public <T> Class<T> getComponentClass(int primitiveID);
    public default <T> T readPrimitive(Class<T> primitiveClass, ByteBuffer buffer) {
        return getPrimitiveSerializer(primitiveClass).readPrimitive(buffer);
    }
    
    public default <T> T readPrimitive(int primitiveID, ByteBuffer buffer) {
        return readPrimitive(getComponentClass(primitiveID), buffer);
    }
    
    public default <T> void writePrimitive(T t, ByteBuffer buffer) {
        getPrimitiveSerializer((Class<T>) t.getClass()).writePrimitive(t, buffer);
    }
    
    public default <T> int getSizeOfPrimitive(T t) {
        return getPrimitiveSerializer((Class<T>) t.getClass()).getSizeOfPrimitive(t);
    }
}