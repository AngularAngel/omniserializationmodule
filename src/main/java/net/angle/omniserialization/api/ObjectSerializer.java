package net.angle.omniserialization.api;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Map;
import lombok.NonNull;

/**
 *
 * @author angle
 */
public interface ObjectSerializer<T, E extends Builder<T>> {
    public @NonNull E instantiateBuilder();
    public Object[] getComponents(@NonNull T instance);
    public void readComponents(@NonNull ObjectSerializerRegistry registry, @NonNull E builder, ArrayList<Builder> builders, @NonNull ByteBuffer buffer);
    public default void writeComponents(@NonNull ObjectSerializerRegistry registry, @NonNull T object, Map<Object, Integer> objPointerMap, @NonNull ByteBuffer buffer) {
        for (Object component : getComponents(object))
            registry.writeComponent(component, objPointerMap, buffer);
    }
    public default int getSizeOfComponents(@NonNull ObjectSerializerRegistry registry, @NonNull T object, Map<Object, Integer> objPointerMap) {
        int size = 0;
        for (Object obj : getComponents(object))
            size += registry.sizeOfComponent(obj, objPointerMap);
        return size;
    }
}