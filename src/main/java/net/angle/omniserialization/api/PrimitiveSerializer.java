package net.angle.omniserialization.api;

import java.nio.ByteBuffer;

/**
 *
 * @author angle
 */
public interface PrimitiveSerializer<T> {
    public T readPrimitive(ByteBuffer buffer);
    public void writePrimitive(T t, ByteBuffer buffer);
    public int getSizeOfPrimitive(T t);
}