package net.angle.omniserialization.impl;

import com.samrj.devil.math.Vec3;
import com.samrj.devil.util.IOUtil;
import java.lang.reflect.Constructor;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.lwjgl.system.MemoryStack;
import net.angle.omniserialization.api.ObjectSerializerRegistry;

/**
 *
 * @author angle
 */
public class BasicSerializerRegistryTest {
    
    @RequiredArgsConstructor
    @EqualsAndHashCode
    @ToString
    public static class TestClass {
        public final int number;
        public final int otherNumber;
        public final byte[] bytes;
        public final String string;
    }
    
    @AllArgsConstructor
    @EqualsAndHashCode
    @ToString
    public static class EnclosingTestClass {
        public TestClass testClass;
        public short[] shorts;
        public boolean[] booleans;
        public Vec3 vec;
        public ArrayList arraylist;
    }
    
    @Test
    public void testReflectiveSerializer() {
        assertDoesNotThrow(new Executable() {
            @Override
            public void execute() throws Throwable {
                try (MemoryStack stack = MemoryStack.stackPush()) {
                    ObjectSerializerRegistry instance = new OmniSerializationModuleImpl().getSerializerRegistry();
                    //instance.register(String.class, new StringSerializer());
                    instance.registerObject(TestClass.class, new ReflectiveSerializer<>((Constructor<TestClass>) TestClass.class.getConstructors()[0],
                            (TestClass testClass) -> {
                                return new Object[] {testClass.number, testClass.otherNumber, testClass.bytes, testClass.string};
                            }));
                    instance.registerObject(EnclosingTestClass.class, new ReflectiveSerializer<>(
                            (Constructor<EnclosingTestClass>) EnclosingTestClass.class.getConstructors()[0],
                            (EnclosingTestClass enclosingTestClass) -> {
                                return new Object[]{enclosingTestClass.testClass, enclosingTestClass.shorts, enclosingTestClass.booleans, enclosingTestClass.vec, enclosingTestClass.arraylist};
                            }
                    ));
                    
                    TestClass testClass = new TestClass(54, 865, new byte[]{8, 34, 1}, "Dfghj");
                    EnclosingTestClass enclTestClass = new EnclosingTestClass(testClass, new short[] {27, 5676}, new boolean[]{true, false, true, true, false, false}, new Vec3(56), new ArrayList());
                    enclTestClass.arraylist.add(1);
                    
                    ByteBuffer buffer = stack.malloc(Math.max(instance.sizeOfObject(testClass), instance.sizeOfObject(enclTestClass)));
                    instance.writeObject(testClass, buffer);
                    buffer.flip();
                    TestClass testOut = instance.readObject(buffer);
                    assertEquals(testClass, testOut);
                    
                    buffer.clear();
                    instance.writeObject(enclTestClass, buffer);
                    buffer.flip();
                    EnclosingTestClass enclOut = instance.readObject(buffer);
                    assertEquals(enclTestClass, enclOut);
                    
                } catch (Throwable t) {
                    throw t;
                }
            }
        });
    }
    
    @Test
    public void testBufferWrites() {
        assertDoesNotThrow(() -> {
            try (MemoryStack stack = MemoryStack.stackPush()) {
                ByteBuffer buffer = stack.malloc(1000);
                IOUtil.writeVLQ(buffer, -1);
                buffer.putInt(54);
                buffer.flip();
                assertEquals(-1, IOUtil.readVLQ(buffer));
                assertEquals(54, buffer.getInt());
            } catch (Throwable t) {
                throw t;
            }
        });
    }
}