package net.angle.omniserialization.impl;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author angle
 */
public class OmniSerializationModuleImplTest {

    /**
     * Test of getSerializerRegistry method, of class OmniSerializationModuleImpl.
     */
    @Test
    public void testGetSerializerRegistry() {
        OmniSerializationModuleImpl instance = new OmniSerializationModuleImpl();
        assertTrue(instance.getSerializerRegistry() instanceof BasicSerializerRegistry);
    }
}