package net.angle.omniserialization.impl;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import net.angle.omniserialization.api.Builder;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author angle
 */
public class ReflectiveBuilderTest {
    @AllArgsConstructor
    @EqualsAndHashCode
    public static class TestClass {
        public int number;
        public String string;
    }
    
    @Test
    public void testReflectiveBuilder() {
        assertDoesNotThrow(() -> {
            ReflectiveBuilder<TestClass> reflectiveBuilder = (ReflectiveBuilder<TestClass>) new ReflectiveBuilder<>(TestClass.class.getConstructors()[0]);
            reflectiveBuilder.setArgument(0, 56);
            reflectiveBuilder.setArgument(1, "???");
            assertEquals(Builder.Status.READY, reflectiveBuilder.getStatus());
            reflectiveBuilder.build();
            assertEquals(Builder.Status.BUILT, reflectiveBuilder.getStatus());
            assertEquals(new TestClass(56, "???"), reflectiveBuilder.getObject());
            reflectiveBuilder.finish();
            assertEquals(Builder.Status.FINISHED, reflectiveBuilder.getStatus());
            assertEquals(new TestClass(56, "???"), reflectiveBuilder.getObject());
        });
    }
    
}